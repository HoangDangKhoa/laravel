{{ Form::open(array('url' => 'calculate')) }}
<table align="center">
    <tr>
        <td>First Operand</td>
        <td>{{Form::text('numone')}}</td>
    </tr>
    <tr>
        <td>Second Operand</td>
        <td>{{Form::text('numtwo')}}</td>
    </tr>

    <tr>
        <td colspan="2" align="center">
            {{Form::submit('Submit!')}}
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">{{$result ?? ''}}</td>
    </tr>
</table>
{{ Form::close() }}