<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('hello', 'HelloController@index');

Route::get('/', function() {
    echo "<h2>This is my Homepage</h2>";
});

Route::get('/about', function() {
    echo "<h2>This is About Page</h2>";
});

Route::get('/contact', function() {
    echo "<h2>This is Contact Page</h2>";
});

Route::get('/test', function() {
    return view('test',['name' => "KhoaMilan"]);
});

Route::get('/student/{name}', function($name) {
    echo "Student Name is " . $name;
});

Route::get('/users/{name?}', function($name = "Hieu gay") {
    echo "User Name is " .$name;
});

Route::get('hello', 'homeController@index');

Route::get('calculate', function() {
    return view('index');
});

Route::post('calculate', 'HelloController@index');